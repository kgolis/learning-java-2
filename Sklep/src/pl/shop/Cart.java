package pl.shop;

import java.util.ArrayList;

public class Cart {

	private ArrayList<Product> productList;
	
	/**
	 * @param listaProduktow
	 */
	public Cart(ArrayList<Product> listaProduktow) {
		this.productList = listaProduktow;
	}
	
	public Cart() {
		this.productList = new ArrayList<Product>();
	}
	
	public ArrayList<Product> getListaProduktow() {
		return productList;
	}

	public void setListaProduktow(ArrayList<Product> productList) {
		this.productList = productList;
	}

	public double getTotalPrice(){
		double totalPrice = 0d;
		for(Product p : productList){
			totalPrice += p.getCenaJednostkowa();
		}
		return totalPrice;
	}
	
	public void addProdukt(Product product){
		productList.add(product);
	}
	
	public void addProdukt(Product product, int quantity){
		for(int i=0;i<quantity;i++){
			
			productList.add(product);
		}
	}
}
