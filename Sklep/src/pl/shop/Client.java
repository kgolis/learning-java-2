package pl.shop;

public class Client extends Person {

	public Client(String imie, String nazwisko, String login) {
		super(imie, nazwisko, login);
	}

	@Override
	public boolean isPracownik() {
		return false;
	}

	@Override
	public String toString() {
		return imie+" "+nazwisko;
	}

}
