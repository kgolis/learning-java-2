package pl.shop;

public abstract class Person {

	protected String imie, nazwisko, login;

	/**
	 * @param imie
	 * @param nazwisko
	 * @param login
	 */
	public Person(String imie, String nazwisko, String login) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.login = login;
	}

	public abstract boolean isPracownik();
	
	public abstract String toString();
}
