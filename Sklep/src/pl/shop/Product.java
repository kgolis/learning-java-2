package pl.shop;

public class Product {

	private String nazwa;
	private int iloscMagazynowa;
	private double cenaJednostkowa;
	
	/**
	 * @param nazwa
	 * @param iloscMagazynowa
	 * @param cenaJednostkowa
	 */
	public Product(String nazwa, int iloscMagazynowa, double cenaJednostkowa) {
		super();
		this.nazwa = nazwa;
		this.iloscMagazynowa = iloscMagazynowa;
		this.cenaJednostkowa = cenaJednostkowa;
	}
	
	@Override
	public String toString() {
		return nazwa + " [stan mag:"+iloscMagazynowa+" szt.] w cenie " + cenaJednostkowa + " z�/szt.";
	}

	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public int getIloscMagazynowa() {
		return iloscMagazynowa;
	}
	public void setIloscMagazynowa(int iloscMagazynowa) {
		this.iloscMagazynowa = iloscMagazynowa;
	}
	public double getCenaJednostkowa() {
		return cenaJednostkowa;
	}
	public void setCenaJednostkowa(double cenaJednostkowa) {
		this.cenaJednostkowa = cenaJednostkowa;
	}
	
	public void zmniejszStanMagazynu(int ilosc){
		this.iloscMagazynowa-=ilosc;
	}
	
	public void zwiekszStanMagazynu(int ilosc){
		this.iloscMagazynowa+=ilosc;
	}
	
}
