package pl.shop;

import java.awt.geom.GeneralPath;
import java.util.*;

public class Program {

	//private static List<Osoba> listaOsob = new ArrayList<Osoba>();
	private static HashMap<String,Person> listaOsob = new HashMap<String,Person>();
	private static List<Product> listaProduktow = new ArrayList<Product>();
	private static Cart koszykProduktow;
		
	public static void main(String[] args) {
		String command;
		boolean isAppClosing = false;
		generateRandomData();
		
		//Logowanie
		while(true){
			command = StringHelper.getString("Podaj login [KONIEC]:");
			if (command.equalsIgnoreCase("koniec")) {
				isAppClosing = true;
				break;
			}
			
			if(searchForPerson(command)){
				System.out.println("Pracownik sklepu. Aplikacja nie obs�uguje pracownik�w sklepu.");
			} else if (!searchForPerson(command)&&(getPerson(command)!=null)) {
				System.out.println("Witaj drogi kliencie ("+getPerson(command).toString()+")");
				koszykProduktow = new Cart();
				break;
			} else {
				System.out.println("Nie znaleziono.");
			}
		}
		
		//Zalogowano klienta - obs�uga sklepu
		while(!isAppClosing){
			String prod ="";
			printAllProducts();
			printKoszyk();
			command = StringHelper.getString("Wpisz nazw� produktu jaki chcesz doda� do koszyka [KONIEC]:");
			if (command.equalsIgnoreCase("koniec")) break;
			else if (getProdukt(command).getNazwa()!=null) {
				if(isProductAvaible(command)){
					prod=command;
					command= StringHelper.getString("Ile chcesz doda�?");
					int ilosc=Integer.parseInt(command);
					if(getProdukt(prod).getIloscMagazynowa()>ilosc){
						getProdukt(prod).zmniejszStanMagazynu(ilosc);
						koszykProduktow.addProdukt(getProdukt(prod),ilosc);
						
					} else System.out.println("W magazynie jest za ma�o produkt�w.");
				}
				
				
			}
			
			
		}
		
	}

	public static Product getProdukt(String nazwa){
		for(Product p : listaProduktow){
			if(p.getNazwa().equals(nazwa)) return p;
		}
		return null;
	}
	
	public static boolean isProductAvaible(String nazwa){
		for(Product p : listaProduktow){
			if(p.getNazwa().equals(nazwa)) {
				if (p.getIloscMagazynowa()>0) return true;
			}
		}
		return false;
	}
	
/*
 Wypisz wszystkie produkty dodane do listy produkt�w. Pobierz od u�ytkownika numer produktu. 
Je�eli u�ytkownik wpisze 0, wyjd� ze sklepu i wypisz warto�� koszyka. 
Je�eli wybierze warto�� wi�ksz� ni� 0, sprawd�, czy wybrany produkt istnieje. 
Je�eli nie istnieje, poinformuj o tym u�ytkownika i popro� o podanie innego produktu. Je�eli produkt istnieje, popro� u�ytkownika o ilo�� produkt�w, 
kt�re chcesz doda� do koszyka. Je�eli nie ma dostatecznie du�o produkt�w w magazynie, poinformuj o tym u�ytkownika i popro� o podanie prawid�owej warto�ci. 
Je�eli jest odpowiednia ilo��, dodaj produkt do koszyka. Odejmij wybran� ilo�� od stanu magazynowego produktu. 
Po dodaniu, wy�wietl warto�� koszyka i wr�� do punktu 7.
 */
	
	
	
	public static boolean searchForPerson(String login){
		if(listaOsob.get(login)!=null){
			return listaOsob.get(login).isPracownik();
		}
		return false;
	}
	public static Person getPerson(String login){
		if(listaOsob.get(login)!=null){
			return listaOsob.get(login);
		}
		return null;	
	}
	public static void printAllProducts(){
		System.out.println("Lista produkt�w w sklepie:");
		for(Product p : listaProduktow){
			System.out.println(p.toString());
		}
	}	
	public static void generateRandomData(){
		//Osoby
		listaOsob.put("JKowal", new Client("Jan","Kowalski","JKowal")); //(new Klient("Jan","Kowalski","JKowal"));
		listaOsob.put("MWes", new Client("Maria","Wesolutka","MWes")); //listaOsob.add(new Klient("Maria","Wesolutka","MWes"));
		listaOsob.put("BBOR", new Employee("Bogdan","Borkowski","BBOR"));
		listaOsob.put("WWIS", new Employee("Wojciech","Wi�niewski","WWIS"));
		//Produkty
		listaProduktow.add(new Product("Mleko",20,1.75d));
		listaProduktow.add(new Product("Bu�ka",50,0.35d));
	}

	public static void printKoszyk(){
		System.out.println("Tw�j koszyk:");
		if(!koszykProduktow.getListaProduktow().isEmpty()){
			List<Product> temp = new ArrayList<Product>(koszykProduktow.getListaProduktow());
			
			for(Product p : temp){
				System.out.println(p.getNazwa()+" ");
			}
		
			
		}
	}
}
